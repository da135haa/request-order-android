package com.example.stevenliu.legend_bun;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity
{
    ImageView imageView1, imageView2, imageView3, imageView4, imageView5, imageView6, imageView7;

    EditText editText1, editText2, editText3, editText4, editText5, editText6, editText7, editTextPhone, editTextRemork, editTextAddress;

    String orderForm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(0xFFFF0011);

        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView6 = (ImageView) findViewById(R.id.imageView6);
        imageView7 = (ImageView) findViewById(R.id.imageView7);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        editText4 = (EditText) findViewById(R.id.editText4);
        editText5 = (EditText) findViewById(R.id.editText5);
        editText6 = (EditText) findViewById(R.id.editText6);
        editText7 = (EditText) findViewById(R.id.editText7);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        editTextRemork = (EditText) findViewById(R.id.editTextRemark);
        editTextAddress = (EditText) findViewById(R.id.editTextAddress);


        Glide.with(imageView1.getContext()).load(R.drawable.a).centerCrop().into(imageView1);
        Glide.with(imageView2.getContext()).load(R.drawable.b).centerCrop().into(imageView2);
        Glide.with(imageView3.getContext()).load(R.drawable.c).centerCrop().into(imageView3);
        Glide.with(imageView4.getContext()).load(R.drawable.d).centerCrop().into(imageView4);
        Glide.with(imageView5.getContext()).load(R.drawable.e).centerCrop().into(imageView5);
        Glide.with(imageView6.getContext()).load(R.drawable.f).centerCrop().into(imageView6);
        Glide.with(imageView7.getContext()).load(R.drawable.g).centerCrop().into(imageView7);

        imageView1.setOnClickListener(clickListener);
        imageView2.setOnClickListener(clickListener);
        imageView3.setOnClickListener(clickListener);
        imageView4.setOnClickListener(clickListener);
        imageView5.setOnClickListener(clickListener);
        imageView6.setOnClickListener(clickListener);
        imageView7.setOnClickListener(clickListener);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(clickListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener clickListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            //關閉虛擬鍵盤
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            switch (view.getId())
            {
                case R.id.fab:
                {
                    if (editTextPhone.getText().length() == 0)
                    {
                        Toast.makeText(MainActivity.this, "電話請記得填寫", Toast.LENGTH_SHORT).show();
                    }
                    else if (editTextAddress.getText().length() == 0)
                    {
                        Toast.makeText(MainActivity.this, "住址請記得填寫", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        int total, allTotal = 0;

                        orderForm = "口味                單價    數量   總價\n";

                        if (editText1.getText().length() == 0)
                        {
                            orderForm += "鮮肉包            14元    0顆    0元\n";
                        }
                        else
                        {

                            total = Integer.parseInt(editText1.getText().toString());
                            allTotal += total * 14;
                            orderForm += "鮮肉包            14元    " + total + "顆    " + (total * 14) + "元\n";
                        }


                        if (editText2.getText().length() == 0)
                        {
                            orderForm += "高麗菜包        14元    0顆    0元\n";
                        }
                        else
                        {

                            total = Integer.parseInt(editText2.getText().toString());
                            allTotal += total * 14;
                            orderForm += "高麗菜包        14元    " + total + "顆    " + (total * 14) + "元\n";
                        }


                        if (editText3.getText().length() == 0)
                        {
                            orderForm += "韭菜粉絲包    14元    0顆    0元\n";
                        }
                        else
                        {

                            total = Integer.parseInt(editText3.getText().toString());
                            allTotal += total * 14;
                            orderForm += "韭菜粉絲包    14元    " + total + "顆    " + (total * 14) + "元\n";
                        }

                        if (editText4.getText().length() == 0)
                        {
                            orderForm += "韭菜肉包        14元    0顆    0元\n";
                        }
                        else
                        {

                            total = Integer.parseInt(editText4.getText().toString());
                            allTotal += total * 14;
                            orderForm += "韭菜肉包        14元    " + total + "顆    " + (total * 14) + "元\n";
                        }

                        if (editText5.getText().length() == 0)
                        {
                            orderForm += "筍子肉包        14元    0顆    0元\n";
                        }
                        else
                        {

                            total = Integer.parseInt(editText5.getText().toString());
                            allTotal += total * 14;
                            orderForm += "筍子肉包        14元    " + total + "顆    " + (total * 14) + "元\n";
                        }

                        if (editText6.getText().length() == 0)
                        {
                            orderForm += "辣子雞肉包    17元    0顆    0元\n";
                        }
                        else
                        {

                            total = Integer.parseInt(editText6.getText().toString());
                            allTotal += total * 17;
                            orderForm += "辣子雞肉包    17元    " + total + "顆    " + (total * 17) + "元\n";
                        }


                        if (editText7.getText().length() == 0)
                        {
                            orderForm += "酸菜肉包        14元    0顆    0元\n";
                        }
                        else
                        {
                            total = Integer.parseInt(editText7.getText().toString());
                            allTotal += total * 14;
                            orderForm += "酸菜肉包        14元    " + total + "顆    " + (total * 14) + "元\n\n";
                        }

                        orderForm += "總金額:" + allTotal + "元\n\n";

                        orderForm += "電話:" + editTextPhone.getText().toString() + "\n";
                        orderForm += "地址:" + editTextAddress.getText().toString() + "\n";
                        if (editTextRemork.getText().length() != 0)
                        {
                            orderForm += "備註:" + editTextRemork.getText().toString();
                        }


                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                        builder.setTitle("訂單確認") //標題文字
                                .setMessage(orderForm) //內容
                                .setPositiveButton("確定", new DialogInterface.OnClickListener()
                                {
                                    @Override //匿名類別DialogInterface.OnClickListener是指定不能更改的
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"da135haa@gmail.com"});
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "包子訂單");
                                        intent.putExtra(Intent.EXTRA_TEXT, orderForm);
                                        intent.setType("message/rfc822"); // MIME data type
                                        MainActivity.this.startActivity(intent);
                                    }
                                });
                        builder.setNegativeButton("取消", null). //按鈕,沒特別指定傾聽者可以設null
                                show(); //顯示
                    }
                    break;
                }
                case R.id.imageView1:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.a).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }

                case R.id.imageView2:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.b).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }

                case R.id.imageView3:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.c).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }

                case R.id.imageView4:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.d).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }

                case R.id.imageView5:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.e).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }

                case R.id.imageView6:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.f).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }

                case R.id.imageView7:
                {
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    final View v = inflater.inflate(R.layout.image_layout, null);
                    ImageView imageView=(ImageView) v.findViewById(R.id.image);

                    Glide.with(imageView.getContext()).load(R.drawable.g).centerCrop().into(imageView);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);//建立物件
                    builder.setView(v);
                    builder.show(); //顯示
                    break;
                }
            }

        }
    };
}
